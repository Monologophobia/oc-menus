let menu_categories_tabs = document.querySelectorAll('#menus .menu-category-tab');
let menu_categories = document.querySelectorAll('#menus .menu-category');
for (var i = 0; i < menu_categories_tabs.length; i++) {
    menu_categories_tabs[i].addEventListener('click', function(e) {
        e.preventDefault();

        for (var x = 0; x < menu_categories_tabs.length; x++) {
            menu_categories_tabs[x].classList.remove('active');
        }
        this.classList.add('active');

        for (var x = 0; x < menu_categories.length; x++) {
            menu_categories[x].classList.remove('active');
        }
        var target = this.hash;
        target = document.querySelector('#menus ' + target);
        target.classList.add('active');

    });
}

var diet_filter = document.getElementById('dietary-requirements');
var menu_items = document.querySelectorAll('#menus .menu .item');
diet_filter.addEventListener('change', function() {
    var checkboxes = diet_filter.querySelectorAll('input[name="diet"]:checked');
    var values = [];
    let checker = (arr, target) => target.every(v => arr.includes(v));
    for (var i = 0; i < checkboxes.length; i++) {
        values.push(checkboxes[i].value);
    }
    for (var i = 0; i < menu_items.length; i++) {
        menu_items[i].classList.remove('hidden');
        if (values.length > 0) {
            var info = JSON.parse(menu_items[i].dataset.diet);
            if (!checker(info, values)) {
                menu_items[i].classList.add('hidden');
            }
        }
    }
});