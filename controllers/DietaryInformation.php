<?php namespace Monologophobia\Restaurant\Controllers;

use Lang;
use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Restaurant\Models\DietaryInformation as DietModel;

class DietaryInformation extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.restaurant'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Restaurant', 'restaurant', 'dietaryinformation');
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            try {
                foreach ($checkedIds as $id) {
                    $record = DietModel::findOrFail($id);
                    $recordry->delete();
                }
                Flash::success(Lang::get('monologophobia.restaurant::lang.misc.deleted'));
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }

            return $this->listRefresh();

        }

    }

}
