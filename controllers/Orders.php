<?php namespace Monologophobia\Restaurant\Controllers;

use Lang;
use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Restaurant\Models\Order;
use Monologophobia\Restaurant\Models\Booking;
use Monologophobia\Restaurant\Models\Settings;

class Orders extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.restaurant'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Restaurant', 'restaurant', 'orders');
    }

    public function index() {
        $order = new Order;
        $this->vars['statuses'] = $order->getStatusOptions();
        parent::index();
    }

    public function live() {
        $order = new Order;
        $this->vars['statuses']   = $order->getStatusOptions();
        $this->vars['pusher_key'] = Settings::get('pusher_key');
        $this->vars['complex']    = Settings::get('complex_orders', false);
        $orders   = Order::where('status', '<', 4)->orderBy('created_at', 'desc')->get();
        $bookings = [];
        foreach ($orders as $order) {
            if (!in_array($order->booking_id, $bookings)) {
                $bookings[] = $order->booking_id;
            }
        }
        $this->vars['bookings'] = Booking::whereIn('id', $bookings)->get();
        $this->vars['orders']   = $orders->groupBy('table_id');
    }

    public function live_onNewOrder() {
        $order_id = intval(post('order_id'));
        $order = Order::findOrFail($order_id);
        $order->items = $order->getItems();
        $order->table_name = $order->order_table->name;
        $order->booking_name = $order->booking->name ?? 'Booking';
        return $order;
    }

    public function live_onUpdateStatus() {
        $order  = Order::findOrFail(intval(post('order_id')));
        $status = intval(post('status'));
        $order->status = $status;
        $order->save();
        return $order;
    }

    public function live_onPayBooking() {
        $booking = Booking::findOrFail(intval(post('booking_id')));
        foreach ($booking->orders as $order) {
            $order->status = 4;
            $order->save();
        }
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            try {
                foreach ($checkedIds as $id) {
                    $order = Order::findOrFail($id);
                    $order->delete();
                }
                Flash::success(Lang::get('monologophobia.restaurant::lang.misc.deleted'));
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }

            return $this->listRefresh();

        }

    }

    public function index_onUpdateStatus() {

        $status = intval(post('action'));
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            try {
                foreach ($checkedIds as $id) {
                    $order = Order::findOrFail($id);
                    $order->status = $status;
                    $order->save();
                }
                Flash::success(Lang::get('monologophobia.restaurant::lang.misc.updated'));
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }

            return $this->listRefresh();

        }

    }

}
