<?php namespace Monologophobia\Restaurant\Controllers;

use Lang;
use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Restaurant\Models\Menu;

class Menus extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relations.yaml';

    public $requiredPermissions = ['monologophobia.restaurant'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Restaurant', 'restaurant', 'menus');
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            try {
                foreach ($checkedIds as $id) {
                    $category = Menu::findOrFail($id);
                    $category->delete();
                }
                Flash::success(Lang::get('monologophobia.restaurant::lang.misc.deleted'));
            }
            catch (\Exception $e) {
                Flash::error($e->getMessage());
            }

            return $this->listRefresh();

        }

    }

}
