<?php namespace Monologophobia\Restaurant\Components;

use Lang;
use Session;
use Redirect;
use DateTime;
use Exception;
use AjaxException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Monologophobia\Restaurant\Models\Menu;
use Monologophobia\Restaurant\Models\Booking;
use Monologophobia\Restaurant\Models\Settings;
use Monologophobia\Restaurant\Models\Category;
use Monologophobia\Restaurant\Models\DietaryInformation;

/**
 * Display Menus 
 *
 */

class DisplayMenus extends ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'monologophobia.restaurant::lang.components.displaymenu.name',
            'description' => 'monologophobia.restaurant::lang.components.displaymenu.description'
        ];
    }

    public function defineProperties() {
        return [
            'css' => [
                'title'   => 'monologophobia.restaurant::lang.components.displaymenu.css',
                'type'    => 'checkbox',  
                'default' => 1,
            ],
            'checkoutpage' => [
                'title'   => Lang::get('monologophobia.restaurant::lang.components.cart.checkoutpage'),
                'type'    => 'dropdown',
                'default' => 0,
            ],
            'menucategory' => [
                'title'   => 'Menu Category',
                'type'    => 'dropdown',
                'default' => 0,
            ],
        ];
    }

    public function getCheckoutPageOptions() {
        $array = [0 => "No checkout"];
        $array = array_merge($array, Page::sortBy('baseFileName')->lists('title', 'url'));
        return $array;
    }

    public function getMenuCategoryOptions() {
        $array = [0 => "All"];
        $array = array_merge($array, Category::lists('name', 'id'));
        return $array;
    }

    public function init() {
        if ($this->property('checkoutpage')) $this->addComponent('Monologophobia\Restaurant\Components\Cart', 'cart', ['checkoutpage' => $this->property('checkoutpage')]);
    }

    public function onRun() {

        $css = intval($this->property('css'));
        if (boolval($css)) $this->addCss('/plugins/monologophobia/restaurant/assets/css/menus.css');
        $this->addJs('/plugins/monologophobia/restaurant/assets/js/menus.js');

        $this->page['dietary_information'] = DietaryInformation::get();
        $this->page['categories'] = $this->property('menucategory') ? Category::where('id', $this->property('menucategory'))->get() : Category::get();
        $this->page['checkout'] = boolval($this->property('checkoutpage'));

        if (Settings::get('complex_orders')) $this->setupComplexOrders();

    }

    private function setupComplexOrders() {

        $this->page['complex'] = true;
        $this->page['booking'] = false;

        $booking = Session::get('booking', false);
        $booking = Booking::where('id', $booking["id"])->first();

        if (!$booking || !$booking->isActive()) {
            Session::forget('booking');
            Session::forget('booking_user');
        }
        else {

            $this->page['booking'] = $booking;

            $booking_user = Session::get('booking_user', false);
            if (!$booking_user) $booking_user = uniqid();
            $this->page['booking_user'] = $booking_user;
            Session::put('booking_user', $booking_user);

            $this->page['pusher_key'] = Settings::get('pusher_key');

        }

    }

    public function onSubmitCode() {
        $code    = post('code');
        $booking = Booking::getByCode($code);
        if ($booking) {
            Session::put('booking', $booking->toArray());
            return Redirect::refresh();
        }
        throw new AjaxException(['code' => 'Booking not found']);
    }

}
