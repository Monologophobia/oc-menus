<?php namespace Monologophobia\Restaurant\Components;

use Lang;
use Session;
use Redirect;
use Response;

use Cms\Classes\Page;

use Monologophobia\Restaurant\Models\Order;
use Monologophobia\Restaurant\Models\Table;
use Monologophobia\Restaurant\Models\Booking;
use Monologophobia\Restaurant\Models\MenuItem;
use Monologophobia\Restaurant\Models\Settings;

class Checkout extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => Lang::get('monologophobia.restaurant::lang.components.checkout.name'),
            'description' => Lang::get('monologophobia.restaurant::lang.components.checkout.description'),
        ];
    }

    public function defineProperties() {
        return [
            'order_id' => [
                'title' => Lang::get('monologophobia.restaurant::lang.components.checkout.order_id'),
                'type'  => 'dropdown',
            ],
        ];
    }
    public function onRun() {

        $order_id = intval($this->property('order_id'));

        $this->page['complex']    = Settings::get('complex_orders');
        $this->page['pusher_key'] = Settings::get('pusher_key');

        if ($this->page['complex']) {
            Session::forget('cart');
            $this->getComplexOrders($order_id);
        }
        else {
            $this->page['order']      = $order_id ? Order::findOrFail($order_id) : false;
            $this->page['cart']       = Session::get('cart');
            $this->page['tables']     = Table::get();
            $this->page['stripe_key'] = boolval(Settings::get('live')) ? Settings::get('publishable') : Settings::get('sandbox_publishable');
        }

    }

    private function getComplexOrders(int $order_id) {
        $booking = Session::get('booking');
        $user    = Session::get('booking_user');
        $orders  = Order::where('booking_id', $booking['id'])->get();
        $yours   = [];
        $others  = [];
        $yours_historical = [];
        foreach ($orders as $order) {
            $order->status = $order->getStatusOptions()[$order->status];
            if ($order->id == $order_id) {
                $yours = $order;
                if ($order->payment_reference != $user) abort(404);
            }
            else {
                if ($order->payment_reference == $user) {
                    $yours_historical[] = $order;
                }
                else {
                    $others[] = $order;
                }
            }
        }
        $this->page['order'] = $yours;
        $this->page['historical'] = $yours_historical;
        $this->page['others'] = $others;
    }

    public function onRemoveFromCart() {
        $id   = intval(post('id'));
        $cart = Session::get('cart');
        foreach ($cart as $key => $item) {
            if ($item["id"] == $id) {
                unset($cart[$key]);
                break;
            }
        }
        Session::put('cart', $cart);
        return Redirect::refresh();
    }

    public function onComplete() {

        $cart    = Session::get('cart');
        $table   = false;
        $booking = false;

        if ($table_id = intval(post('table_id'))) {
            $table = Table::findOrFail($table_id);
        }
        if ($code = post('booking_reference', false)) {
            $booking = Booking::where('code', $code)->firstOrFail();
            $table   = $booking->table;
        }

        $order = new Order();
        $items = [];
        $total = 0;
        $order->notes = filter_var(post('notes'), FILTER_SANITIZE_STRING);
        $order->items = [];
        $order->table_id = $table ? $table->id : null;
        $order->booking_id = $booking ? $booking->id : null;
        $order->status   = 0;
        foreach ($cart as $item) {
            $items[] = $item["id"];
            $total  += $item["price"];
        }
        $order->items = $items;
        $order->total = $total;

        $amount_in_pence = intval($order->total * 100);
        $description = "Food Order";
        $payment = $this->takePayment(post('payment_method_id'), post('payment_intent_id'), $amount_in_pence, $description);
        if ($payment->requires_action || $payment->error) {
            return Response::json($payment);
        }
        else {
            $order->payment_reference = $payment->payment->payment_id;
        }

        $order->save();

        Session::forget('cart');

        return Redirect::to(url()->current() . "/" . $order->id);

    }

    private function takePayment($method = false, $intent = false, int $amount, string $description) {

        if (!$method && !$intent) throw new Exception('Method or Intent must be specified');

        $live = boolval(Settings::get('live'));
        $key  = ($live ? Settings::get('secret') : Settings::get('sandbox_secret'));
        \Stripe\Stripe::setApiKey($key);

        $payment_intent = false;
        $return = ['requires_action' => false, 'payment_intent_client_secret' => false, 'error' => false, 'payment' => false];

        // Do intent retrieval first as that is more important on the second round trip
        if ($intent) {
            $payment_intent = \Stripe\PaymentIntent::retrieve($intent);
            if ($payment_intent->status == "requires_confirmation") $payment_intent->confirm();
        }
        else if ($method) {

            $payment_array = [
                'amount'      => $amount,
                'currency'    => 'GBP',
                'confirm'     => true,
                'description' => $description,
                'payment_method' => $method,
                'confirmation_method' => 'automatic',
            ];

            $payment_intent = \Stripe\PaymentIntent::create($payment_array);

        }

        if ($payment_intent->status == 'requires_action' && $payment_intent->next_action->type == 'use_stripe_sdk') {
            $return['requires_action'] = true;
            $return['payment_intent_client_secret'] = $payment_intent->client_secret;
        }
        else if ($payment_intent->status == 'succeeded') {
            $return['payment'] = $payment_intent;
        }

        return (object) $return;

    }

    public function onUpdateStatus() {

        $order_id = intval(post('order_id'));
        $order    = Order::where('id', $order_id);

        if (Settings::get('complex_orders')) {
            $booking = Session::get('booking');
            $order->where('booking_id', $booking['id']);
        }

        $order = $order->firstOrFail();
        $order->status = $order->getStatusOptions()[$order->status];

        return $order;

    }

    public function onUpdate() {
        $order_id = intval(post('order_id'));
        $this->getComplexOrders($order_id);
        return [
            '#complex-details' => $this->renderPartial('@details')
        ];
    }

    
}
