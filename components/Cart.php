<?php namespace Monologophobia\Restaurant\Components;

use Lang;
use Session;

use Cms\Classes\Page;

use Monologophobia\Restaurant\Models\Order;
use Monologophobia\Restaurant\Models\Booking;
use Monologophobia\Restaurant\Models\MenuItem;
use Monologophobia\Restaurant\Models\Settings;

class Cart extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => Lang::get('monologophobia.restaurant::lang.components.cart.name'),
            'description' => Lang::get('monologophobia.restaurant::lang.components.cart.description'),
        ];
    }

    public function defineProperties() {
        return [
            'checkoutpage' => [
                'title' => Lang::get('monologophobia.restaurant::lang.components.cart.checkoutpage'),
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getCheckoutPageOptions() {
        return Page::sortBy('baseFileName')->lists('title', 'url');
    }

    public function onRun() {
        $cart = Session::get('cart');
        if (!$cart) {
            $cart = [];
            Session::put('cart', $cart);
        }
        $this->page['cart'] = $cart;
        $this->page['checkout_page'] = $this->property('checkoutpage');
        $this->page['complex'] = Settings::get('complex_orders');
    }

    public function onAddToCart() {
        $id   = intval(post('id'));
        $item = MenuItem::findOrFail($id);
        Session::push('cart', $item->toArray());
        $this->page['cart'] = Session::get('cart');
        return [
            '#cart' => $this->renderPartial('cart::cart')
        ];
    }

    public function onRemoveFromCart() {
        $id   = intval(post('id'));
        $cart = Session::get('cart');
        foreach ($cart as $key => $item) {
            if ($item["id"] == $id) {
                unset($cart[$key]);
                break;
            }
        }
        Session::put('cart', $cart);
        $this->page['cart'] = Session::get('cart');
        return [
            '#cart' => $this->renderPartial('cart::cart')
        ];
    }

    public function onComplexCheckout() {
        $booking = Session::get('booking');
        $booking = Booking::findOrFail($booking['id']);
        $cart    = Session::get('cart');
        $order = new Order();
        $items = [];
        $total = 0;
        $order->notes = filter_var(post('notes'), FILTER_SANITIZE_STRING);
        $order->items = [];
        $order->table_id = $booking->order_table->id;
        $order->booking_id = $booking->id;
        $order->status   = 0;
        foreach ($cart as $item) {
            $items[] = $item["id"];
            $total  += $item["price"];
        }
        $order->items = $items;
        $order->total = $total;
        $order->payment_reference = Session::get('booking_user');
        $order->save();
        return \Redirect::to("/checkout/{$order->id}");
    }

}
