<?php namespace Monologophobia\Restaurant\Models;

use \October\Rain\Database\Model;
use Monologophobia\Restaurant\Models\DietaryInformation;

class MenuItem extends Model {

    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    public $table = 'mono_menu_items';
    public $timestamps = true;

    protected $nullable = ['description', 'dietary_information'];
    protected $jsonable = ['dietary_information'];

    public $rules = [
        'name'  => 'required|string',
        'price' => 'required|numeric',
    ];

    public $belongsTo = [
        'menu' => ['Monologophobia\Restaurant\Models\Menu', 'key' => 'menu_id']
    ];

    public function getDietaryInformationOptions() {
        return DietaryInformation::lists('name', 'name');
    }

}
