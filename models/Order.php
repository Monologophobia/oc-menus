<?php namespace Monologophobia\Restaurant\Models;

use \October\Rain\Database\Model;
use Monologophobia\Restaurant\Models\MenuItem;

class Order extends Model {

    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    public $table = 'mono_restaurant_orders';
    public $timestamps = true;

    protected $jsonable = ['items'];
    protected $nullable = ['notes', 'payment_reference'];

    public $rules = [
        'total' => 'required|numeric',
    ];

    public $belongsTo = [
        'order_table' => ['Monologophobia\Restaurant\Models\Table', 'key' => 'table_id'],
        'booking' => ['Monologophobia\Restaurant\Models\Booking', 'key' => 'booking_id'],
    ];

    public function getStatusOptions() {
        $complete = Settings::get('complex_orders', false) ? 'Paid' : 'Complete';
        return [
            0 => 'Pending',
            1 => 'Acknowledged',
            2 => 'Being Prepared',
            3 => 'At Table',
            4 => $complete,
        ];
    }

    public function getItems() {
        $array = [];
        foreach ($this->items as $item_id) {
            $array[] = MenuItem::find($item_id);
        }
        return $array;
    }

    public function afterCreate() {
        $pusher  = Settings::makePusher();
        $complex = Settings::get('complex_orders', false);
        if ($pusher) {
            $pusher->trigger('orders', 'created', $this->id);
            if ($complex) $pusher->trigger('bookings', 'order-created', $this->booking->id);
        }
    }

    public function afterUpdate() {
        $pusher  = Settings::makePusher();
        $complex = Settings::get('complex_orders', false);
        if ($pusher) {
            $pusher->trigger('orders', 'updated', $this->id);
            if ($complex) $pusher->trigger('bookings', 'order-updated', $this->booking->id);
        }
    }

    public function getItemCountAttribute() {
        return count($this->items);
    }

}
