<?php namespace Monologophobia\Restaurant\Models;

use DateTime;
use \October\Rain\Database\Model;

class Table extends Model {

    public $table = 'mono_restaurant_tables';

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'   => 'required|string',
        'people' => 'required|integer',
        'booking_length' => 'required|integer',
    ];

    public $hasMany = [
        'bookings' => ['Monologophobia\Restaurant\Models\Booking', 'key' => 'table_id', 'delete' => true],
        'orders' => ['Monologophobia\Restaurant\Models\Order', 'key' => 'table_id', 'delete' => true],
    ];

    /**
     * Check if the table is available at the specified time
     * Checks for bookings before and after the booking length
     * ie. If booking_length = 120 and the datetime is 12:00
     * then check if there are any bookings between 10:00 and 14:00
     * @param DateTime
     * @return boolean
     */
    public function isAvailable(DateTime $datetime) {
        // Are there any bookings before or after this datetime that would interfere
        $before = clone $datetime;
        $after  = clone $datetime;
        $before->modify("-$this->booking_length minutes");
        $after->modify("+$this->booking_length minutes");
        $bookings = $this->bookings()->where('datetime', '>=', $before)->where('datetime', '<=', $after)->first();
        return !$bookings;
    }

    public function getAvailableTimes(DateTime $datetime) {

    }

}
