<?php namespace Monologophobia\Restaurant\Models;

use \October\Rain\Database\Model;
use Monologophobia\Restaurant\Models\DietaryInformation;

class Menu extends Model {

    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    public $table = 'mono_menus';

    protected $nullable = ['preamble', 'footer'];

    public $rules = [
        'name' => 'required|string',
        'category_id' => 'required|integer'
    ];

    public $belongsTo = [
        'category' => ['Monologophobia\Restaurant\Models\Category', 'key' => 'category_id']
    ];

    public $hasMany = [
        'items' => ['Monologophobia\Restaurant\Models\MenuItem', 'key' => 'menu_id', 'delete' => true, 'order' => 'asc']
    ];

    public $attachOne = [
        'image' => ['System\Models\File'],
        'pdf' => ['System\Models\File']
    ];

}
