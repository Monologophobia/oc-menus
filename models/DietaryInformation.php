<?php namespace Monologophobia\Restaurant\Models;

use \October\Rain\Database\Model;

class DietaryInformation extends Model {

    public $table = 'mono_restaurant_dietary_information';

    protected $nullable = ['description'];

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string',
    ];

}
