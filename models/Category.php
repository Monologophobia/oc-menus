<?php namespace Monologophobia\Restaurant\Models;

use \October\Rain\Database\Model;

class Category extends Model {

    public $table = 'mono_restaurant_menus_categories';

    protected $nullable = ['description'];

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string'
    ];

    public $belongsTo = [
        'parent' => ['Monologophobia\Restaurant\Models\Category', 'key' => 'parent_id']
    ];

    public $hasMany = [
        'menus' => ['Monologophobia\Restaurant\Models\Menu', 'key' => 'category_id']
    ];

    public $attachOne = [
        'image' => ['System\Models\File']
    ];

    public function getParentOptions() {
        $return = [0 => "None"];
        $categories = Category::get();
        foreach ($categories as $category) {
            if ($category->id != $this->id) {
                $return[$category->id] = $category->name;
            }
        }
        return $return;
    }

}
