<?php namespace Monologophobia\Restaurant\Models;

use Model;

class Settings extends Model {

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'mono_restaurant_settings';
    public $settingsFields = 'fields.yaml';

    public static function makePusher() {
        $pusher_app_id = Settings::get('pusher_app_id');
        $pusher_key    = Settings::get('pusher_key');
        $pusher_secret = Settings::get('pusher_secret');
        if ($pusher_app_id && $pusher_key && $pusher_secret) {
            return new \Pusher\Pusher( $pusher_key, $pusher_secret, $pusher_app_id, array('cluster' => "eu") );
        }
        return false;
    }

}
