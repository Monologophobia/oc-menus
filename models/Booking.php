<?php namespace Monologophobia\Restaurant\Models;

use Lang;
use DateTime;
use ValidationException;
use \October\Rain\Database\Model;

class Booking extends Model {

    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    public $table = 'mono_restaurant_bookings';

    protected $jsonable = ['people'];
    protected $nullable = ['code'];
    protected $dates    = ['datetime'];

    public $rules = [
        'name'     => 'required|string',
        'datetime' => 'required|date',
    ];

    public $belongsTo = [
        'order_table' => ['Monologophobia\Restaurant\Models\Table', 'key' => 'table_id']
    ];

    public $hasMany = [
        'orders' => ['Monologophobia\Restaurant\Models\Order', 'key' => 'booking_id', 'delete' => true]
    ];

    public function filterFields($fields, $context = null) {
        $complex_orders = Settings::get('complex_orders');
        //$fields->code->hidden = !$complex_orders;
        if ($complex_orders && $context == 'create') {
            $fields->code->value = self::getToken();
        }
    }

    public function afterValidate() {

        // If we're using the multi-customer booking instead of the one-to-one
        // verify that the code we want to use is unique for the day and past all previous 
        // bookings
        if (Settings::get('complex_orders')) {
            $now = new DateTime();
            $now->setTime(0, 0, 0);
            // bookings on the day of this one
            $bookings = Booking::where('datetime', '>=', $this->datetime->format('Y-m-d H:i:s'))->get();
            // only get the active bookings
            foreach ($bookings as $key => $booking) {
                if (!$booking->isActive()) {
                    $bookings->forget($key);
                }
            }
            // make sure the code is unique
            foreach ($bookings as $booking) {
                if ($booking->code == $this->code) {
                    throw new ValidationException(['code' => Lang::get('monologophobia.restaurant::lang.bookings.code_collision')]);
                }
            }
        }

    }

    /**
     * Gets a single booking by its booking code
     * Only searches todays bookings
     * Checks the isActive() method to make sure the booking... is... active...
     * @param string code
     * @return Booking or false
     */
    public static function getByCode(string $code) {
        $today = new DateTime();
        $today->setTime(0, 0, 0);
        $bookings = Booking::where('code', $code)->where('datetime', '>=', $today->format('Y-m-d H:i:s'))->get();
        foreach ($bookings as $booking) {
            if ($booking->isActive()) {
                return $booking;
            }
        }
        return false;
    }

    /**
     * Check if we're now past the table's booking duration
     * @return boolean
     */
    public function isActive() {
        $now = new DateTime();
        $booking_datetime = clone $this->datetime;
        $booking_datetime->modify("+{$this->order_table->booking_length} minutes");
        return $now < $booking_datetime;
    }

    /**
     * Gets a random 6 character alphanumeric code
     * @return string
     */
    public static function getToken() : string {

        $token  = "";

        //$code   = Lang::get('monologophobia.restaurant::lang.misc.uppercase_alphabet');
        $code   = Lang::get('monologophobia.restaurant::lang.misc.lowercase_alphabet');
        $code  .= Lang::get('monologophobia.restaurant::lang.misc.digits');
        $length = strlen($code);

        // six digits
        for ($i = 0; $i < 6; $i++) {
            $token .= $code[self::cryptographicallyRandomSecureCode(0, $length - 1)];
        }

        return $token;

    }

    /**
     * Generates a random string which is more secure than uniqid
     * I'm not smart enough to understand this as it was found at 
     * https://stackoverflow.com/questions/1846202/php-how-to-generate-a-random-unique-alphanumeric-string/13733588#13733588
     * @param int
     * @param int
     * @return string
     */
    public static function cryptographicallyRandomSecureCode($min, $max) {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

}
