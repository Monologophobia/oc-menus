<?php namespace Monologophobia\Restaurant\Classes;

use Session;

class SessionMiddleware {

    public function handle($request, Closure $next) {

        $user    = Session::get('booking_user');
        $booking = Session::get('booking');
        $request->merge(['booking_user' => $user, 'booking' => $booking]);

        return $next($request);

    }

}