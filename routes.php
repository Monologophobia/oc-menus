<?php

use Session;
use Illuminate\Http\Request;
use Monologophobia\Restaurant\Models\Booking;

Route::middleware('Monologophobia\Restaurant\Classes\SessionMiddleware')->get('pusher/auth', function(Request $request) {
    $socket_id    = post('socket_id');
    $channel_name = post('channel_name');
    dd($request->booking_user);
});