<?php namespace Monologophobia\Restaurant\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointTwoPointZero extends Migration {

    public function up() {

        Schema::create('mono_restaurant_dietary_information', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('mono_restaurant_menus_categories', function($table) {
            $table->integer('parent_id')->unsigned()->nullable();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_restaurant_dietary_information');
        Schema::table('mono_restaurant_menus_categories', function($table) {
            $table->dropColumn('parent_id');
        });
    }

}
