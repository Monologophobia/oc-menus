<?php namespace Monologophobia\Restaurant\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointFivePointZero extends Migration {

    public function up() {

        Schema::table('mono_restaurant_bookings', function($table) {
            $table->string('code')->nullable();
        });

    }

    public function down() {
        Schema::table('mono_restaurant_bookings', function($table) {
            $table->dropColumn('code');
        });
    }

}
