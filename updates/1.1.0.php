<?php namespace Monologophobia\Restaurant\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointOnePointZero extends Migration {

    public function up() {

        Schema::create('mono_restaurant_tables', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('people')->default(2);
            $table->integer('booking_length')->default(120);
            $table->timestamps();
        });

        Schema::create('mono_restaurant_bookings', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('table_id')->index()->unsigned();
            $table->foreign('table_id')->references('id')->on('mono_restaurant_tables')->onDelete('cascade');
            $table->text('people');
            $table->datetime('datetime');
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_restaurant_bookings');
        Schema::dropIfExists('mono_restaurant_tables');
    }

}
