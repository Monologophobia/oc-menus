<?php namespace Monologophobia\Restaurant\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointZero extends Migration {

    public function up() {

        Schema::create('mono_restaurant_menus_categories', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('mono_menus', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->index()->unsigned();
            $table->foreign('category_id')->references('id')->on('mono_restaurant_menus_categories')->onDelete('cascade');
            $table->text('preamble')->nullable();
            $table->text('content');
            $table->text('footer')->nullable();
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_menus');
        Schema::dropIfExists('mono_restaurant_menus_categories');
    }

}
