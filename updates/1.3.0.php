<?php namespace Monologophobia\Restaurant\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointThreePointZero extends Migration {

    public function up() {

        Schema::table('mono_menus', function($table) {
            $table->dropColumn('content');
        });

        Schema::create('mono_menu_items', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->double('price', 8, 2);
            $table->text('description')->nullable();
            $table->text('dietary_information')->nullable();
            $table->integer('menu_id')->index()->unsigned()->nullable();
            $table->foreign('menu_id')->references('id')->on('mono_menus')->onDelete('cascade');
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::table('mono_menus', function($table) {
            $table->text('content');
        });
        Schema::dropIfExists('mono_menu_items');
    }

}
