<?php namespace Monologophobia\Restaurant\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointFourPointZero extends Migration {

    public function up() {

        Schema::create('mono_restaurant_orders', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('table_id')->index()->unsigned();
            $table->foreign('table_id')->references('id')->on('mono_restaurant_tables')->onDelete('cascade');
            $table->integer('booking_id')->index()->unsigned()->nullable();
            $table->foreign('booking_id')->references('id')->on('mono_restaurant_bookings')->onDelete('cascade');
            $table->text('items');
            $table->text('notes')->nullable();
            $table->double('total', 8, 2);
            $table->integer('status')->default(0);
            $table->string('payment_reference')->nullable();
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_restaurant_orders');
    }

}
