# Food and Drink Menu system for October CMS

Allows an end-user to upload or manage Menus either as pre-made PDFs or directly as text. Includes a front-end component to display all Menus in a category or a specific Menu. This component also includes the option to use some pre-made CSS to make the implementation of the Plugin relatively easy.

![Frontend](https://gitlab.com/Monologophobia/oc-menus/tree/master/assets/Frontend.jpg)
![Backend](https://gitlab.com/Monologophobia/oc-menus/tree/master/assets/Backend.jpg)