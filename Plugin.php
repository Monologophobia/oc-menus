<?php namespace Monologophobia\Restaurant;

use Lang;
use Backend;
use System\Classes\PluginBase;

/**
 * Shop Plugin Information File
 */
class Plugin extends PluginBase {

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'monologophobia.restaurant::lang.plugin.name',
            'description' => 'monologophobia.restaurant::lang.plugin.description',
            'author'      => 'Monologophobia',
            'icon'        => 'icon-shopping-cart'
        ];
    }

    // Register plugin permissions
    public function registerPermissions() {
        return [
            'monologophobia.restaurant' => ['tab' => 'monologophobia.restaurant::lang.plugin.name', 'label' => 'monologophobia.restaurant::lang.plugin.permissions'],
        ];
    }

    // Create the backend navigation
    public function registerNavigation() {
        return [
            'restaurant' => [
                'label'       => 'Restaurant',
                'url'         => Backend::url('monologophobia/restaurant/menus'),
                'icon'        => 'icon-cutlery',
                'permissions' => ['monologophobia.restaurant'],
                'order'       => 300,

                'sideMenu' => [
                    'bookings' => [
                        'label'       => 'Bookings',
                        'url'         => Backend::url('monologophobia/restaurant/bookings'),
                        'icon'        => 'icon-users',
                        'permissions' => ['monologophobia.restaurant']
                    ],
                    'orders' => [
                        'label'       => 'Orders',
                        'url'         => Backend::url('monologophobia/restaurant/orders/live'),
                        'icon'        => 'icon-credit-card',
                        'permissions' => ['monologophobia.restaurant']
                    ],
                    'menus' => [
                        'label'       => Lang::get('monologophobia.restaurant::lang.plugin.name'),
                        'url'         => Backend::url('monologophobia/restaurant/menus'),
                        'icon'        => 'icon-cutlery',
                        'permissions' => ['monologophobia.restaurant']
                    ],
                ]

            ]
        ];
    }

    // Register front-end components
    public function registerComponents() {
        return [
           '\Monologophobia\Restaurant\Components\DisplayMenus' => 'displaymenus',
           '\Monologophobia\Restaurant\Components\Cart' => 'cart',
           '\Monologophobia\Restaurant\Components\Checkout' => 'checkout',
        ];
    }

    public function registerPageSnippets() {
        return [
            '\Monologophobia\Restaurant\Components\DisplayMenus' => 'displaymenu',
        ];
    }

    public function registerSettings() {
        return [
            'settings' => [
                'label'       => 'monologophobia.restaurant::lang.plugin.name',
                'description' => 'monologophobia.restaurant::lang.plugin.description',
                'category'    => 'monologophobia.restaurant::lang.plugin.name',
                'icon'        => 'icon-cutlery',
                'class'       => 'Monologophobia\Restaurant\Models\Settings',
            ]
        ];
    }

}
